﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Company.DAL.Models
{
    public enum JobTitle
    {
        Administrator,
        Developer,
        Architect,
        Manager
    }

    public class Employe
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey("Company")]
        public long CompanyId { get; set; }
        [Required] [MaxLength(120)]
        public string FirstName { get; set; }
        [Required] [MaxLength(120)]
        public string LastName { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public JobTitle JobTitle { get; set; }
        [Required]
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }


        public virtual Company Company { get; set; }
    }
}
