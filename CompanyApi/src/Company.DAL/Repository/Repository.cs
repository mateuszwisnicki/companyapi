﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Company.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Company.DAL.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly MainContext _mainContext;

        public Repository(MainContext mainContext)
        {
            _mainContext = mainContext;
        }

        public virtual IQueryable<T> GetQueryable()
        {
            return _mainContext.Set<T>();
        }


        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = _mainContext.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            var list = dbQuery
                .AsNoTracking()
                .ToList<T>();

            return list;
        }

        public virtual IList<T> GetList(Func<T, bool> where,
            params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;

            IQueryable<T> dbQuery = _mainContext.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            list = dbQuery
                .AsNoTracking()
                .Where(where)
                .ToList<T>();

            return list;
        }

        public virtual T GetSingle(Func<T, bool> where,
            params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;

            IQueryable<T> dbQuery = _mainContext.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            item = dbQuery
                .AsNoTracking() //Don't track any changes for the selected item
                .FirstOrDefault(where); //Apply where clause

            return item;
        }


        public virtual void Add(params T[] items)
        {
            foreach (T item in items)
            {
                _mainContext.Entry(item).State = EntityState.Added;
            }

            _mainContext.SaveChanges();
        }

        public virtual void Add(T item)
        {
            _mainContext.Entry(item).State = EntityState.Added;

            _mainContext.SaveChanges();
        }

        public void AddList(params T[] items)
        {
            foreach (T item in items)
            {
                _mainContext.Entry(item).State = EntityState.Added;
            }

            _mainContext.SaveChanges();
        }

        public void AddOrUpdateList(params T[] items)
        {
            foreach (T item in items)
            {
            }
        }


        public virtual void Update(params T[] items)
        {
            foreach (T item in items)
            {
                _mainContext.Entry(item).State = EntityState.Modified;
            }

            _mainContext.SaveChanges();
        }


        public virtual void Remove(params T[] items)
        {
            foreach (T item in items)
            {
                _mainContext.Entry(item).State = EntityState.Deleted;
            }

            _mainContext.SaveChanges();
        }

        public bool Exist(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = _mainContext.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            return dbQuery
                .AsNoTracking() //Don't track any changes for the selected item
                .Any(where); //Apply where clause
        }
    }
}
