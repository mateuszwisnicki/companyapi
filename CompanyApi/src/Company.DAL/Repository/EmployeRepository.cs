﻿using Company.DAL.Models;

namespace Company.DAL.Repository
{
    public class EmployeRepository : Repository<Employe>, IEmployeRepository
    {
        private readonly MainContext _mainContext;

        public EmployeRepository(MainContext mainContext)
            : base(mainContext)
        {
            _mainContext = mainContext;
        }
    }
}