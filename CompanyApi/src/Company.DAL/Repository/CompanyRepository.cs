﻿using Company.DAL.Models;

namespace Company.DAL.Repository
{
    public class CompanyRepository : Repository<Models.Company>, ICompanyRepository
    {
        private readonly MainContext _mainContext;

        public CompanyRepository(MainContext mainContext)
            : base(mainContext)
        {
            _mainContext = mainContext;
        }
    }
}