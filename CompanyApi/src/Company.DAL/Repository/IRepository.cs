﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Company.DAL.Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetQueryable();
        IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);
        IList<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        T GetSingle(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        bool Exist(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);
        void Add(params T[] items);
        void Add(T item);
        void AddList(params T[] items);
        void Update(params T[] items);
        void Remove(params T[] items);
        void AddOrUpdateList(params T[] items);
    }
}
