﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.DAL.Repository
{
    public interface ICompanyRepository:IRepository<Models.Company>
    {

    }
}
